package test.entity;

import mvc.actioninterface.AbstractBean;
import mvc.entity.ActionBean;

public class LoginBean extends AbstractBean{
	
	private String name;
	private String password;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
