package mvc.entity;

import java.util.Map;

public class ActionBean {
	private String name;
	
	private String actionclass;
	
	private String path;
	private String actionForm;
	
	
	
	public String getActionForm() {
		return actionForm;
	}

	public void setActionForm(String actionForm) {
		this.actionForm = actionForm;
	}

	private Map<String, String> map;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getActionclass() {
		return actionclass;
	}

	public void setActionclass(String actionclass) {
		this.actionclass = actionclass;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

	@Override
	public String toString() {
		return "ActionBean [name=" + name + ", actionclass=" + actionclass
				+ ", path=" + path + ", map=" + map + "]";
	}
	
	
}
