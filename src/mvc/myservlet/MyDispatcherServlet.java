package mvc.myservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.actioninterface.AbstractBean;
import mvc.actioninterface.ActionForm;
import mvc.entity.ActionBean;
import mvc.utill.Analysisdom;
import mvc.utill.FullAction;
import mvc.utill.GoAction;

/**
 * 得到**.do可获取**，通过解析path可以获取javabeam的class名，然后根据属性名，把值赋值给属性。
 * 然后再把对象传入excute方法的参数中，接着再根据返回值与xml的name对应，若相同返回该页面，否则报错。
 * @author Administrator
 *
 */
public class MyDispatcherServlet extends HttpServlet {

	public MyDispatcherServlet() {
		super();
	}


	public void destroy() {
		super.destroy();
	}


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	
	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Map<String, ActionBean> bean = Analysisdom.getBean(request);
		System.out.println(bean.get("login").toString());
		String servletPath=request.getServletPath();
		int indexOf = servletPath.indexOf(".");
		
		String substring = servletPath.substring(1,indexOf);
		System.out.println(substring);
		ActionBean aBean=null;
		if ((aBean = bean.get(substring))!=null) {
			String actionclass = aBean.getActionclass();
			AbstractBean fullaction = FullAction.fullaction(request, actionclass);
			ActionForm acForm = GoAction.getAcForm(aBean.getActionForm());
			String excute = acForm.excute(request, response, fullaction);
			if (!(excute.equals(null)&&excute.equals(""))) {
				request.getRequestDispatcher(aBean.getMap().get(excute)).forward(request, response);
				
			}
			
		}else {
			//throw new Exception("路径不匹配");
			System.out.println("路径不匹配");
		}
		
	}


	public void init() throws ServletException {
	}

}
