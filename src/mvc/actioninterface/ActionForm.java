package mvc.actioninterface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.entity.ActionBean;

public interface ActionForm {
	public String excute(HttpServletRequest request,HttpServletResponse response,AbstractBean aBean);
}
