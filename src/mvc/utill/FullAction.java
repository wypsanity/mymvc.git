package mvc.utill;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import mvc.actioninterface.AbstractBean;

public class FullAction {
	public static AbstractBean fullaction(HttpServletRequest request,String actionclass){
		AbstractBean newInstance =null;
		try {
			Class<?> forName = Class.forName(actionclass);
			newInstance = (AbstractBean)forName.newInstance();
			Field[] declaredFields = forName.getDeclaredFields();
			for (Field field : declaredFields) {
				/*System.out.println(field.getName());*/
				String parameter = request.getParameter(field.getName());
				if (!field.isAccessible()) {
					field.setAccessible(true);
				}
				field.set(newInstance, parameter);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newInstance;
		
		
	}
	public static void main(String[] args) {
		try {
			Class<?> forName = Class.forName("test.entity.LoginBean");
			Field[] declaredFields = forName.getDeclaredFields();
			for (Field field : declaredFields) {
				System.out.println(field.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
